<?php

namespace Test\Unit\Service;

use PHPUnit\Framework\TestCase;
use App\Entity\Mapping;
use App\Service\Directory;

class DirectoryTest extends TestCase
{
    /**
     * Directory Service
     *
     * @var Directory
     */
    private $directoryService;

    public function setUp()
    {
        $this->directoryService = new Directory();
    }

    /**
     * Test if local directory correct generated.
     *
     * @param string $lmsPath
     * @param string $localPath
     * @param string $expectation
     *
     * @dataProvider generateLocalDirectoryDataProvider
     */
    public function testGenerateLocalDirectory($lmsPath, $localPath, $expectation)
    {
        $mappingEntity = new Mapping();
        $mappingEntity->setLocalPath($localPath)
            ->setLmsPath($lmsPath);

        $this->assertSame(
            $expectation,
            $this->directoryService->generateLocalDirectory($mappingEntity)
        );
    }

    public function generateLocalDirectoryDataProvider()
    {
        yield 'nothing to generate' => [
            'lms_path' => '/path/to/lms/file.mp3',
            'local_path' => '/path/to/local/fileNew.mp3',
            'expectation' => $this->generateAbsolutePathWithEnvironemntVar('/path/to/local/fileNew.mp3')
        ];

        yield 'empty lms path' => [
            'lms_path' => '',
            'local_path' => '',
            'expecation' => $this->generateAbsolutePathWithEnvironemntVar('/')
        ];

        yield 'generate local path by lms file name' => [
            'lms_path' => '/path/to/lms/file.mp3',
            'local_path' => '',
            'expectation' => $this->generateAbsolutePathWithEnvironemntVar('/file.mp3')
        ];

        yield 'dont generate if rootpath already set' => [
            'lms_path' => $this->generateAbsolutePathWithEnvironemntVar('/var/upload/test.mp3'),
            'local_path' => $this->generateAbsolutePathWithEnvironemntVar('/test/eintrag/test/file.mp3'),
            'expectation' => $this->generateAbsolutePathWithEnvironemntVar('/test/eintrag/test/file.mp3')
        ];
    }

    private function generateAbsolutePathWithEnvironemntVar($relativePath)
    {
        return Directory::generateMediaRootPath().$relativePath;
    }
}