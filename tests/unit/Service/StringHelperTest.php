<?php
namespace Test\Unit\Service;

use PHPUnit\Framework\TestCase;
use App\Service\StringHelper;

class StringHelperTest extends TestCase
{
    /**
     * Instanze of string helper
     *
     * @var StringHelper
     */
    private $stringHelper;

    public function setUp() {
        $this->stringHelper = new StringHelper();
    }

    /**
     * @dataProvider replaceQuotesDataProvider
     */
    public function testReplaceQuotes($target, $expectation)
    {
        $this->assertSame($expectation, $this->stringHelper->removeQuotes($target));
    }

    public function replaceQuotesDataProvider()
    {
        yield 'replace single quotes on begin' => [
            '\'testEintrag',
            'testEintrag'
        ];

        yield 'replace single quotes on ' => [
            'testEintrag\'',
            'testEintrag'
        ];

        yield 'replace single quotes on many positions' => [
            '\'t\'est\'Ei\'n\'trag\'',
            'testEintrag'
        ];

        yield 'replace string with only single quote' => [
            '\'',
            ''
        ];

        yield 'replace double quotes on begin' => [
            '"testEintrag',
            'testEintrag'
        ];

        yield 'replace double quotes on end' => [
            'testEintrag"',
            'testEintrag'
        ];

        yield 'replace double quotes on many positions' => [
            't"es"t""Ein"t"r"a"g"',
            'testEintrag'
        ];

        yield 'replace string with only double quote' => [
            '"',
            ''
        ];

        yield 'replace nothing' => [
            'testEintrag',
            'testEintrag'
        ];

        yield 'replace nothing in empty string' => [
            '',
            ''
        ];
    }

    public function testReplaceQuotesOnNull()
    {
        $this->expectException(\TypeError::class);
        $this->expectExceptionMessage('removeQuotes() must be of the type string, null given, called in');
        
        $this->stringHelper->removeQuotes(null);
    }
}