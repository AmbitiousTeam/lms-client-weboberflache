<?php

namespace Test\Unit\Service;
use PHPUnit\Framework\TestCase;
use App\Service\KeyToArrayConverter;

class KeyToArrayConverterTest extends TestCase
{
    private $keyToArrayConverter = null;

    public function setUp()
    {
        $this->keyToArrayConverter = new KeyToArrayConverter();
    }

    public function testSimpleKey()
    {
        $this->assertSame(["key" => "value"], $this->keyToArrayConverter->convert([], "key", "value"));
    }

    public function testSubArrayKey()
    {
        $expectedArray = [
            'key' => [
                'subKey' => 'value'
            ]
        ];

        $this->assertSame($expectedArray, $this->keyToArrayConverter->convert([], "key['subKey']", "value"));
    }

    public function testEmptyContent()
    {
        $this->assertSame(['' => ''], $this->keyToArrayConverter->convert([], "", ""));
    }

    public function testExtendExistingArrayWithSubKey()
    {
        $originalArray = [
            'key' => [
                'id' => 1
            ]
        ];

        $expectedArray = [
            'key' => [
                'id' => 1,
                'name' => 'value'
            ]
        ];

        $this->assertSame($expectedArray, $this->keyToArrayConverter->convert($originalArray, "key['name']", "value"));
    }

    public function testExtendExistingArrayWithSubKeyWithoutQuotes()
    {
        $originalArray = [
            'key' => [
                'id' => 1
            ]
        ];

        $expectedArray = [
            'key' => [
                'id' => 1,
                'name' => 'value'
            ]
        ];

        $this->assertSame($expectedArray, $this->keyToArrayConverter->convert($originalArray, "key[name]", "value"));
    }

    public function testExtendExistingArrayWithOtherKey()
    {
        $originalArray = [
            'key' => [
                'id' => 1
            ]
        ];

        $expectedArray = [
            'key' => [
                'id' => 1
            ],
            'secondKey' => [
                'name' => 'value'
            ]
        ];

        $this->assertSame($expectedArray, $this->keyToArrayConverter->convert($originalArray, "secondKey[name]", "value"));
    }
}