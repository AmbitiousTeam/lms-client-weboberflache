<?php
/**
 * Service für Key und Array Operationen
 *
 * PHP Version 7
 *
 * @category   PHP
 * @package    LmsClient
 * @subpackage Service
 * @author     Andreas Kempe <andreas.kempe@byte-artist.de>
 * @copyright  2019 Andreas Kempe
 * @license    GPL http://opensource.org/licenses/gpl-license.php GNU Public License
 * @version    GIT:
 * @link       http://lms-client.byte-artist.de
 */
namespace App\Service;

class KeyToArrayConverter
{
    /**
     * Convert given name (key) and content in array, extends given data array.
     * This is mostly used to convert given post strings like mapping[id]=1 
     * in Form conform array.
     *
     * @param [array]  $data
     * @param [string] $name
     * @param [string] $content
     * 
     * @return array
     */
    public function convert(array $data, string $name, string $content) : array
    {
        if (preg_match('/^([0-9A-Za-z]+)(\[.*\])/', $name, $matches)) {
            $key = StringHelper::removeQuotes($matches[1]);
            if (0 < strlen($matches[2])) {
                $data[$key] = $this->convert(array_key_exists($key, $data) ? $data[$key] : [], $matches[2], $content);
            } else {
                $data[$key] = $content;
            }
        } elseif (preg_match('/^\[([^\]]+)\](.*)/', $name, $matches)) {
            $key = StringHelper::removeQuotes($matches[1]);
            if (0 < strlen($matches[2])) {
                $data[$key] = $this->convert(array_key_exists($key, $data) ? $data[$key] : [], $matches[2], $content);
            } else {
                $data[$key] = $content;
            }
        } else {
            $data[$name] = $content;
        }
        return $data;
    }
}
