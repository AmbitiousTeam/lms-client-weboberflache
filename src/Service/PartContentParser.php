<?php
/**
 * Klasse enthält funktionalitäten für das parsen von boundary contante.
 *
 * PHP Version 7
 * 
 * @category   PHP
 * @package    LmsClient
 * @subpackage Service
 * @author     Andreas Kempe <andreas.kempe@byte-artist.de>
 * @copyright  2019 Andreas Kempe
 * @license    GPL http://opensource.org/licenses/gpl-license.php GNU Public License
 * @version    GIT: 
 * @link       http://lms-client.byte-artist.de
 */
namespace App\Service;

class PartContentParser
{
    public function parse($content) 
    {
        $boundary = substr($content, 0, strpos($content, "\r\n"));

        // Fetch each part
        $parts = array_slice(explode($boundary, $content), 1);
        $data = array();
        $keyToArrayConverter = new KeyToArrayConverter();
        
        foreach ($parts as $part) {
            // If this is the last part, break
            if ($part == "--\r\n") break; 
        
            // Separate content from headers
            $part = ltrim($part, "\r\n");
            list($raw_headers, $body) = explode("\r\n\r\n", $part, 2);
        
            // Parse the headers list
            $raw_headers = explode("\r\n", $raw_headers);
            $headers = array();
            foreach ($raw_headers as $header) {
                list($name, $value) = explode(':', $header);
                $headers[strtolower($name)] = ltrim($value, ' '); 
            } 
        
            // Parse the Content-Disposition to get the field name, etc.
            if (isset($headers['content-disposition'])) {
                $filename = null;
                preg_match(
                    '/^(.+); *name="([^"]+)"(; *filename="([^"]+)")?/', 
                    $headers['content-disposition'], 
                    $matches
                );
                list(, $type, $name) = $matches;
                isset($matches[4]) and $filename = $matches[4]; 
        
                // handle your fields here
                switch ($name) {
                    // this is a file upload
                    case 'userfile':
                         file_put_contents($filename, $body);
                         break;
        
                    // default for all other files is to populate $data
                    default: 
                         $data = $keyToArrayConverter->convert($data, $name, substr($body, 0, strlen($body) - 2));
                         break;
                } 
            }
        }
        return $data;
    }
}