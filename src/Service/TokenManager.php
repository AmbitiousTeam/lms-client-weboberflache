<?php
/**
 * Service token management.
 *
 * PHP Version 7
 *
 * @category   PHP
 * @package    LmsClient
 * @subpackage Service
 * @author     Andreas Kempe <andreas.kempe@byte-artist.de>
 * @copyright  2019 Andreas Kempe
 * @license    GPL http://opensource.org/licenses/gpl-license.php GNU Public License
 * @version    GIT:
 * @link       http://lms-client.byte-artist.de
 */
namespace App\Service;

use App\Entity\Progress;
use App\Entity\Mapping;
use App\Exceptions\InvalidTokenException;

class TokenManager
{
    /**
     * Token Directory
     *
     * @var string
     */
    private $tokenDir;

    /**
     * Service to generate local directory from given mapping entity.
     *
     * @var Directory
     */
    private $directoryService;

    public function __construct(Directory $directoryService, string $tokenDir) 
    {
        $this->tokenDir = $tokenDir;
        $this->directoryService = $directoryService;
    }

    public function create(Mapping $mappingEntity, $fileSize) 
    {
        $this->prepare($this->tokenDir);
        $token = $this->generateToken();
        $localPath = $this->directoryService->generateLocalDirectory($mappingEntity);

        $progressEntity = new Progress();
        $progressEntity->setOriginalPathName($mappingEntity->getLocalPath())
            ->setSize($fileSize)
            ->setFilePathName($localPath);

        /** Delete possible existing file because progress returns 100%, if a old file exists */
        if (file_exists($progressEntity->getFilePathName())
            && is_file($progressEntity->getFilePathName())
        ) {
            unlink($progressEntity->getFilePathName());
        }

        $this->update($token, $progressEntity);
        return $token;
    }

    public function update($token, Progress $progressEntity)
    {
        file_put_contents(
            $this->tokenDir.'/'.$token,
            serialize($progressEntity)
        );
        return $this;
    }

    public function read($token) 
    {
        return unserialize(file_get_contents($this->tokenDir.'/'.$token));
    }

    public function delete($token) 
    {
        return unlink($this->tokenDir.'/'.$token);
    }

    private function prepare(string $tokenPath) 
    {
        if (!is_dir($tokenPath)) {
            mkdir($tokenPath, 0777, true);
        }
        return $this;
    }

    private function generateToken() 
    {
        return uniqid('lms_client_');
    }
    
    public function validate($token)
    {
        if (!preg_match('/lms_client_[0-9a-z]{13}/', $token)
//            || !is_readable($this->tokenDir.'/'.$token)
        ) {
            throw new InvalidTokenException('Given token "'.$token.'" invalid or not exists!');
        }
    }
}
