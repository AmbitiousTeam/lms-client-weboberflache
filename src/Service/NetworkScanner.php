<?php
/**
 * Klasse enthält funktionalitäten für den Scan im Netzwerk.
 *
 * PHP Version 7
 *
 * @category   PHP
 * @package    LmsClient
 * @subpackage Service
 * @author     Andreas Kempe <andreas.kempe@byte-artist.de>
 * @copyright  2019 Andreas Kempe
 * @license    GPL http://opensource.org/licenses/gpl-license.php GNU Public License
 * @version    GIT:
 * @link       http://lms-client.byte-artist.de
 */
namespace App\Service;

use Psr\Log\LoggerInterface;

class NetworkScanner
{
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function scanMacForIp($ip)
    {
        $scanCommand = "ip neigh show ".$ip;

        $result = exec($scanCommand, $output, $exitCode);

        if ($exitCode) {
            $this->logger->error("IP Adresse ".$ip." konnte nicht gescannt werden! FehlerCode: ".$exitCode);
            return [];
        }

        $regEx = '/^'.
            '(?<ip>[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}) '.
            '(?<env>.*?) '.
            '(?<interface>.*?) '.
            '(?<lladdr>.*?) '.
            '(?<mac>[0-9a-f]{2}:[0-9a-f]{2}:[0-9a-f]{2}:[0-9a-f]{2}:[0-9a-f]{2}:[0-9a-f]{2}) '.
            '(?<stale>.*)/';
        
        if (preg_match($regEx, $output[0], $matches)) {
            return $matches;
        }
        return [];
    }
}
