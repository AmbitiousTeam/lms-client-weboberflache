<?php
/**
 * Klasse enthält funktionalitäten für das verarbeiten des upload progress per token.
 * 
 * PHP Version 7
 * 
 * @category   PHP
 * @package    LmsClient
 * @subpackage Service
 * @author     Andreas Kempe <andreas.kempe@byte-artist.de>
 * @copyright  2019 Andreas Kempe
 * @license    GPL http://opensource.org/licenses/gpl-license.php GNU Public License
 * @version    GIT: 
 * @link       http://lms-client.byte-artist.de
 */
namespace App\Service;

use App\Entity\Progress;

class ProgressManager
{
    /**
     * Service for Token Management.
     *
     * @var TokenManager
     */
    private $tokenManager;

    public function __construct(TokenManager $tokenManager)
    {
        $this->tokenManager = $tokenManager;
    }

    /**
     * Calculate Progress for given Token.
     *
     * @param string $token
     * 
     * @return void
     */
    public function calculateProgress($token)
    {
        /** @var Progress $progressEntity */
        $progressEntity = $this->tokenManager->read($token);

        if (is_readable($progressEntity->getFilePathName())
            && is_file($progressEntity->getFilePathName())
        ) {
            $progressEntity->setCurrentSize(filesize($progressEntity->getFilePathName()));
        }

        $this->calculatePercentage($progressEntity);

        $this->tokenManager->update($token, $progressEntity);

        return $progressEntity;
    }

    private function calculatePercentage(Progress $progressEntity) 
    {
        if (0 < $progressEntity->getSize()) {
            $progressEntity->setPercentage(
                number_format(
                    ($progressEntity->getCurrentSize() * 100) / $progressEntity->getSize(),
                    2
                )
            );
        }
        return $this;
    }
}
