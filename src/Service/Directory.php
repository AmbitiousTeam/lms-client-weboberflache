<?php
/**
 * Service für Verzeichnis Operationen
 * 
 * PHP Version 7
 * 
 * @category   PHP
 * @package    LmsClient
 * @subpackage Service
 * @author     Andreas Kempe <andreas.kempe@byte-artist.de>
 * @copyright  2019 Andreas Kempe
 * @license    GPL http://opensource.org/licenses/gpl-license.php GNU Public License
 * @version    GIT: 
 * @link       http://lms-client.byte-artist.de
 */
namespace App\Service;

use App\Entity\Mapping;

class Directory
{
    public static function generateLocalDirectory(Mapping $mappingEntity)
    {
        $filePathName = $mappingEntity->getLocalPath();

        if (empty($filePathName)) {
            $filePathName = basename($mappingEntity->getLmsPath());
        }

        $fileName = basename($filePathName);
        $filePath = dirname($filePathName);

        // online file name without path
        if ('.' === $filePath) {
            $filePath = '';
        }

        $mediaRootPath = static::generateMediaRootPath();

        if (0 === strpos($filePathName, $mediaRootPath)) {
            return $filePathName;
        }

        if ($filePath != $filePathName) {
            $mediaRootPath .= $filePath;
        }

        if (!is_dir($mediaRootPath)) {
            mkdir($mediaRootPath, 0777, true);
        }

        return $mediaRootPath.'/'.$fileName;
    }

    public static function generateMediaRootPath()
    {
        $mediaRootPath = $_ENV['MEDIA_PATH'];

        if (empty($mediaRootPath)) {
            $mediaRootPath = __DIR__.'/var';
        }

        return $mediaRootPath;
    }
}