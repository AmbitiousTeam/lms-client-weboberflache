<?php
/**
 * Service for string functions.
 *
 * PHP Version 7
 *
 * @category   PHP
 * @package    LmsClient
 * @subpackage Service
 * @author     Andreas Kempe <andreas.kempe@byte-artist.de>
 * @copyright  2019 Andreas Kempe
 * @license    GPL http://opensource.org/licenses/gpl-license.php GNU Public License
 * @version    GIT:
 * @link       http://lms-client.byte-artist.de
 */
namespace App\Service;

class StringHelper
{
    /**
     * Function remove Quotes from given string.
     *
     * @param string $string
     * @return void
     */
    public static function removeQuotes(string $string) : string
    {
        return preg_replace('/\'|"/', '', $string);
    }
}
