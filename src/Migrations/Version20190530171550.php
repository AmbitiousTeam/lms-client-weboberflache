<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190530171550 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Insert initial Values';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('INSERT INTO `media_types` (`name`) VALUES (\'File\'), (\'Playlist\'), (\'Folder\')');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DELETE `media_types` FROM `media_types` WHERE name = \'File\' OR name = \'Playlist\' OR name = \'Folder\'');
    }
}
