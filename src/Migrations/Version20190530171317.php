<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190530171317 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Creates Initial Database Schema';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE clients (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL UNIQUE, ip TEXT DEFAULT NULL, mac_address TEXT DEFAULT NULL)');

        $this->addSql('CREATE TABLE media_types (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL UNIQUE)');

        $this->addSql('CREATE TABLE mappings (id INTEGER PRIMARY KEY AUTOINCREMENT, media_type INTEGER UNSIGNED DEFAULT NULL, client INTEGER UNSIGNED DEFAULT NULL, rfid TEXT NOT NULL, additional_information TEXT NOT NULL, local_path TEXT NOT NULL, lms_path TEXT NOT NULL)');

        // this up() migration is auto-generated, please modify it to your needs
        /*
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE clients (id INT UNSIGNED AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL COLLATE utf8mb4_0900_ai_ci, ip VARCHAR(15) DEFAULT NULL COLLATE utf8mb4_0900_ai_ci, mac_address VARCHAR(50) DEFAULT NULL COLLATE utf8mb4_0900_ai_ci, UNIQUE INDEX name (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE mappings (id INT AUTO_INCREMENT NOT NULL, media_type INT UNSIGNED DEFAULT NULL, client INT UNSIGNED DEFAULT NULL, rfid VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, additional_information VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, local_path VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, lms_path VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, INDEX media_type (media_type), INDEX client (client), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE media_types (id INT UNSIGNED AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL COLLATE utf8mb4_0900_ai_ci, UNIQUE INDEX name (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        */
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE clients');

        $this->addSql('DROP TABLE media_types');

        $this->addSql('DROP TABLE mappings');
    }
}
