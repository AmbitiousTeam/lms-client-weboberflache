<?php
/**
 * Klasse für die Persistierung der Media Types
 *
 * PHP Version 7
 *
 * @category   PHP
 * @package    LmsClient
 * @subpackage Entity
 * @author     Andreas Kempe <andreas.kempe@byte-artist.de>
 * @copyright  2019 Andreas Kempe
 * @license    GPL http://opensource.org/licenses/gpl-license.php GNU Public License
 * @version    GIT:
 * @link       http://lms-client.byte-artist.de
 */
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MediaTypes
 *
 * @ORM\Table(name="media_types", uniqueConstraints={@ORM\UniqueConstraint(name="unx_name", columns={"name"})})
 * @ORM\Entity
 */
class MediaType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @return int
     */
    public function getId():? int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return MediaType
     */
    public function setId(int $id): MediaType
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName():? string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return MediaType
     */
    public function setName(string $name): MediaType
    {
        $this->name = $name;
        return $this;
    }
}
