<?php
/**
 * Klasse für die Persistierung des Progress
 *
 * PHP Version 7
 *
 * @category   PHP
 * @package    LmsClient
 * @subpackage Entity
 * @author     Andreas Kempe <andreas.kempe@byte-artist.de>
 * @copyright  2019 Andreas Kempe
 * @license    GPL http://opensource.org/licenses/gpl-license.php GNU Public License
 * @version    GIT: 
 * @link       http://lms-client.byte-artist.de
 */
namespace App\Entity;

class Progress implements \Serializable
{
    /**
     * Final Filesize of tranfered file.
     *
     * @var integer
     */
    private $size = 0;

    /**
     * Current Size of transfered file.
     *
     * @var integer
     */
    private $currentSize = 0;

    /**
     * Percentage of transfer state.
     *
     * @var integer
     */
    private $percentage = 0;

    /**
     * Final Pathname for local file.
     *
     * @var string
     */
    private $filePathName = null;

    /**
     * Pathname, provided via Post Request
     *
     * @var string
     */
    private $originalPathName = null;

    /**
     * Pathname if the file is uploading
     *
     * @var string
     */
    private $tempPathName = null;

    public function setSize($size) {
        $this->size = $size;
        return $this;
    }

    public function getSize() {
        return $this->size;
    }

    public function setCurrentSize($currentSize) {
        $this->currentSize = $currentSize;
        return $this;
    }

    public function getCurrentSize() {
        return $this->currentSize;
    }

    public function setPercentage($percentage) {
        $this->percentage = $percentage;
        return $this;
    }

    public function getPercentage() {
        return $this->percentage;
    }

    /**
     * Set file path with name to local file
     *
     * @param string $filePathName
     * 
     * @return Progress
     */
    public function setFilePathName($filePathName) {
        $this->filePathName = $filePathName;
        return $this;
    }

    public function getFilePathName() {
        return $this->filePathName;
    }

    /**
     * Set given File path name from request
     *
     * @param string $originalPathName
     * 
     * @return Progress
     */
    public function setOriginalPathName(?string $originalPathName) {
        $this->originalPathName = $originalPathName;
        return $this;
    }

    public function getOrignalPathName() {
        return $this->originalPathName;
    }

    /**
     * Set temporary file path name from upload
     *
     * @param string $tempPathName
     * 
     * @return Progress
     */
    public function setTempPathName($tempPathName)
    {
        $this->tempPathName = $tempPathName;
        return $this;
    }

    public function getTempPathName() {
        return $this->tempPathName;
    }

    public function serialize()
    {
        return serialize(
            [
                $this->size,
                $this->currentSize,
                $this->percentage,
                $this->filePathName,
                $this->originalPathName,
                $this->tempPathName
            ]
        );
    }

    public function unserialize($serialized)
    {
        list(
            $this->size,
            $this->currentSize,
            $this->percentage,
            $this->filePathName,
            $this->originalPathName,
            $this->tempPathName
        ) = unserialize($serialized);
    }
}