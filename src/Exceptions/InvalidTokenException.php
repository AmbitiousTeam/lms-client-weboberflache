<?php
/**
 * Klasse für Invalid Token Exceptions
 * 
 * PHP Version 7
 * 
 * @category   PHP
 * @package    LmsClient
 * @subpackage Exception
 * @author     Andreas Kempe <andreas.kempe@byte-artist.de>
 * @copyright  2019 Andreas Kempe
 * @license    GPL http://opensource.org/licenses/gpl-license.php GNU Public License
 * @version    GIT: 
 * @link       http://lms-client.byte-artist.de
 */
namespace App\Exceptions;

class InvalidTokenException extends \Exception
{
    
}
