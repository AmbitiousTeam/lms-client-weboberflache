<?php
/**
 * Klasse für die Verarbeitung von MappingTypes
 *
 * PHP Version 7
 *
 * @category   PHP
 * @package    LmsClient
 * @subpackage Controller
 * @author     Andreas Kempe <andreas.kempe@byte-artist.de>
 * @copyright  2019 Andreas Kempe
 * @license    GPL http://opensource.org/licenses/gpl-license.php GNU Public License
 * @version    GIT:
 * @link       http://lms-client.byte-artist.de
 */
namespace App\Controller;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\MediaType;
use Symfony\Component\HttpFoundation\Response;

/**
 * Transfer Controller
 * @Route("/api", name="api_")
 */
class MediaTypeController extends AbstractFOSRestController
{
    /**
     * List all MediaTypes
     *
     * @Rest\Get("/media-types")
     * @Route(name="media-types")
     *
     * @return Response
     */
    public function indexAction()
    {
        $repository = $this->getDoctrine()->getRepository(MediaType::class);
        $mediaTypes = $repository->findAll();

        return $this->handleView(
            $mediaTypes ?
                $this->view($mediaTypes, Response::HTTP_OK) :
                $this->view([], Response::HTTP_NO_CONTENT)
        );
    }
}
