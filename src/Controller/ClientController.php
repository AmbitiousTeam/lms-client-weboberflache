<?php
/**
 * Klasse für die Verarbeitung von Clients
 *
 * PHP Version 7
 *
 * @category   PHP
 * @package    LmsClient
 * @subpackage Controller
 * @author     Andreas Kempe <andreas.kempe@byte-artist.de>
 * @copyright  2019 Andreas Kempe
 * @license    GPL http://opensource.org/licenses/gpl-license.php GNU Public License
 * @version    GIT:
 * @link       http://lms-client.byte-artist.de
 */
namespace App\Controller;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use App\Service\NetworkScanner;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Client;
use Symfony\Component\HttpFoundation\Request;
use App\Form\ClientType;
use App\Service\PartContentParser;
use Psr\Log\LoggerInterface;

/**
 * Client Controller
 * @Route("/api", name="api_")
 */
class ClientController extends AbstractFOSRestController
{
    /**
     * Return detail information for client
     *
     * @Rest\Get("/client/detail")
     * @Route(name="api_client_detail")
     *
     * @return JsonResponse
     */
    public function detailAction(NetworkScanner $networkScanner)
    {
        // Wirkliche IP des aktuellen Rechners
//        $currentIp = $_SERVER['SERVER_ADDR'];
        // IP und eventueller Port, unter dem angefragt wurde
        $currentIp = $_SERVER['HTTP_HOST'];
        $repository = $this->getDoctrine()->getRepository(Client::class);
        $client = $repository->findOneBy(['ip' => $currentIp]);

        if ($client) {
            $client->setName($_ENV['CLIENT_NAME']);
            return new JsonResponse($client, JsonResponse::HTTP_OK);
        }

        $networkResult = $networkScanner->scanMacForIp($currentIp);

        if ($networkResult) {
            $client['name'] = $_ENV['CLIENT_NAME'];
            $client['ip'] = $currentIp;
            $client['macAddress'] = $networkResult['mac'];
        } else {
            $client['name'] = $_ENV['CLIENT_NAME'];
            $client['ip'] = $_SERVER['HTTP_HOST'];
            $client['macAddress'] = '00:00:00:00';
        }
        return new JsonResponse($client, JsonResponse::HTTP_NOT_FOUND);
    }

    /**
     * Insert given detail information.
     *
     * @Rest\Post("/client")
     * @Route(name="api_client_insert")
     *
     * @return JsonResponse
     */
    public function insertAction(LoggerInterface $logger, Request $request)
    {
        $clientEntity = new Client();

        $form = $this->createForm(ClientType::class, $clientEntity);
        $form->submit($request->request->get($form->getName()));

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($clientEntity);
                $entityManager->flush();

                return new JsonResponse($form->getData(), JsonResponse::HTTP_OK);
            } catch (\Exception $exception) {
                $logger->error("MySQL Error!".$exception->getMessage());
            }
        }
        return new JsonResponse(["Nicht eingefügt! : ".$form->getErrors(true, false)], JsonResponse::HTTP_NOT_ACCEPTABLE);
    }

    /**
     * Update given detail information.
     *
     * @Rest\Put("/client")
     * @Route(name="api_client_update")
     *
     * @return JsonResponse
     */
    public function updateAction(PartContentParser $partContentParser, Request $request)
    {
        /*
        $entityManager = $this->getDoctrine()->getManager();
        $clientEntity = $entityManager->getRepository(ClientType::class)->find($request->request->get('id'));

        $form = $this->createForm(ClientType::class, $clientEntity);
        $form->submit($request->request->get($form->getName()));
        */

        $data = $partContentParser->parse($request->getContent())['client'];
        $id = $data['id'];

        $entityManager = $this->getDoctrine()->getManager();
        $clientEntity = $entityManager->getRepository(Client::class)->find($id);

        $form = $this->createForm(ClientType::class, $clientEntity);
        $form->submit($data);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($clientEntity);
            $entityManager->flush();
            return new JsonResponse('ok', JsonResponse::HTTP_OK);
        }
        return new JsonResponse(["Nicht eingefügt! : ".$form->getErrors()]);
    }
}
