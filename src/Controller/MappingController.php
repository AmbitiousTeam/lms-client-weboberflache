<?php
/**
 * Klasse für die Verarbeitung von Mappings
 *
 * PHP Version 7
 *
 * @category   PHP
 * @package    LmsClient
 * @subpackage Controller
 * @author     Andreas Kempe <andreas.kempe@byte-artist.de>
 * @copyright  2019 Andreas Kempe
 * @license    GPL http://opensource.org/licenses/gpl-license.php GNU Public License
 * @version    GIT:
 * @link       http://lms-client.byte-artist.de
 */
namespace App\Controller;

use App\Entity\Mapping;
use App\Form\MappingType;
use App\Service\PartContentParser;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Psr\Log\LoggerInterface;
use App\Service\TokenManager;
use App\Entity\Client;
use App\Entity\MediaType;
use App\Service\Directory;

/**
 * Mapping Controller
 * @Route("/api", name="api_")
 */
class MappingController extends AbstractFOSRestController
{
    /**
     * List all Mappings
     *
     * @Rest\Get("/mappings")
     * @Route(name="mappings")
     *
     * @return Response
     */
    public function indexAction()
    {
        $repository = $this->getDoctrine()->getRepository(Mapping::class);
        $mappings = $repository->findAll();

        return $this->handleView(
            $mappings ?
                $this->view($mappings, Response::HTTP_OK) :
                $this->view([], Response::HTTP_OK)
        );
    }

    /**
     * Load specific mapping by given id.
     *
     * @Rest\Get("/mapping/{id}")
     * @Route(name="load_mapping")
     *
     * @param int $id
     *
     * @return Response
     */
    public function loadMappingAction($id)
    {
        $repository = $this->getDoctrine()->getRepository(Mapping::class);
        $mapping = $repository->find($id);

        return $this->handleView(
            $mapping ?
                $this->view($mapping, Response::HTTP_OK):
                $this->view([], Response::HTTP_NO_CONTENT)
        );
    }

    /**
     * Save given Mapping
     *
     * @Rest\Post("/mapping")
     * @Route(name="create_mapping")
     *
     * @return Response
     */
    public function createAction(Request $request)
    {
        $data = $request->get('mapping');
        $data['client'] = null;

        $mapping = new Mapping();
        $form = $this->createForm(MappingType::class, $mapping);
        $form->submit($data);

        $entityManager = $this->getDoctrine()->getManager();

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($mapping);
            $entityManager->flush();

            return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_CREATED));
        }
        return $this->handleView($this->view([$form->getErrors()]));
    }

    /**
     * Update given Mapping
     *
     * @Rest\Put("/mapping")
     * @Route(name="update_mapping")
     *
     * @return Response
     */
    public function updateAction(LoggerInterface $logger, Directory $directory, PartContentParser $partContentParser, Request $request)
    {
        $data = $request->get('mapping');
        $entityManager = $this->getDoctrine()->getManager();

        // "Fallback" Put should every time send via boundary content,
        // but seems ajax -> json does not do this every time
        if (!is_array($data)
            || empty($data)
        ) {
            $data = $partContentParser->parse($request->getContent())['mapping'];
        }
        
        $logger->debug("MappingData: ".print_r($data, true));

        $clientId = $data['client'];
        $client = $entityManager->getRepository(Client::class)->find($clientId);

        $mediaTypeId = $data['media_type'];
        $mediaType = $entityManager->getRepository(MediaType::class)->find($mediaTypeId);

        $id = $data['id'];
        $mapping = $entityManager->getRepository(Mapping::class)->find($id);

        if (! $mapping instanceof Mapping) {
            $logger->info("Mapping im Update des Client nicht gefunden! Lege neues Mapping an!");
            $mapping = new Mapping();
        }

        /** For generating LocalPath is Mapping Entity needed, so we create temp Entity */
        $directoryMappingEntity = new Mapping();
        $directoryMappingEntity->setLmsPath($data['lms_path']);
        $directoryMappingEntity->setLocalPath($data['local_path']);
        $data['local_path'] = $directory->generateLocalDirectory($directoryMappingEntity);

        $mapping->setClient($client);
        $mapping->setMediaType($mediaType);
        
        $form = $this->createForm(MappingType::class, $mapping);
        $form->submit($data);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $entityManager->persist($mapping);
                $entityManager->flush();

                return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_ACCEPTED));
            } catch (\Exception $exception) {
                $logger->critical($exception->getMessage());
                return $this->handleView($this->view(['status' => 'ok', 'content' => $exception->getMessage()], Response::HTTP_NOT_ACCEPTABLE));
            }
        }

        return $this->handleView($this->view($form->getErrors()));
    }

    /**
     * Delete given Mapping
     *
     * @Rest\Delete("/mapping")
     * @Route(name="delete_mapping")
     *
     * @return Response
     */
    public function deleteAction(Request $request)
    {
        $id = $request->get('id');

        $entityManager = $this->getDoctrine()->getManager();
        $mapping = $entityManager->getRepository(Mapping::class)->find($id);

        if ($mapping) {
            $entityManager->remove($mapping);
            $entityManager->flush();

            return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_ACCEPTED));
        }
        return $this->handleView($this->view(['status' => 'error'], Response::HTTP_NOT_FOUND));
    }

    /**
     * Old function, formely planned as file transfer, but i think better way is here
     * to request from other server and insert or update and resend the synced data.
     *
     * underconstruction.
     *
     * @Rest\Post("/mapping/sync")
     * @Route(name="mapping_sync")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function syncAction(LoggerInterface $logger, TokenManager $tokenManager, Request $request)
    {
        $mappingRequest = $request->get('mapping');
        $entityManager = $this->getDoctrine()->getManager();

        if ($mappingRequest['id']) {
            $mappingEntity = $entityManager->getRepository(Mapping::class)
                ->find($mappingRequest['id']);
        }

        if (! $mappingEntity instanceof Mapping) {
            $mappingEntity = new Mapping();
        }

        $form = $this->createForm(MappingType::class, $mappingEntity);
        $form->submit($request);

        if ($form->isValid()) {
            $filePathName = $mappingEntity->getLocalPath();

            if (empty($filePathName)) {
                $filePathName = basename($mappingEntity->getLmsPath());
            }

            $fileName = basename($filePathName);
            $filePath = dirname($filePathName);

            // online file name without path
            if ('.' === $filePath) {
                $filePath = '';
            }
            $mediaRootPath = $_ENV['MEDIA_PATH'];

            if ($filePath != $filePathName) {
                $mediaRootPath .= $filePath;
            }

            if (!is_dir($mediaRootPath)) {
                mkdir($mediaRootPath, 0777, true);
            }

            $mappingEntity->setLocalPath($mediaRootPath.'/'.$fileName);

#            $fileHandle = fopen($mapping->getLocalPath(), 'w+');
#            fwrite($fileHandle, $content);
#            fclose($fileHandle);

            $entityManager->persist($mappingEntity);
            $entityManager->flush();
        }
        return $this->handleView($this->view(['token' => $tokenManager->create($mappingEntity->getLocalPath())]));
    }
}
