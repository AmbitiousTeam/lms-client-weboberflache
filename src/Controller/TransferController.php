<?php
/**
 * Klasse für die Verarbeitung von Transfers
 *
 * PHP Version 7
 *
 * @category   PHP
 * @package    LmsClient
 * @subpackage Controller
 * @author     Andreas Kempe <andreas.kempe@byte-artist.de>
 * @copyright  2019 Andreas Kempe
 * @license    GPL http://opensource.org/licenses/gpl-license.php GNU Public License
 * @version    GIT:
 * @link       http://lms-client.byte-artist.de
 */
namespace App\Controller;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\TokenManager;
use App\Service\ProgressManager;
use Symfony\Component\HttpFoundation\Request;
use Psr\Log\LoggerInterface;
use App\Entity\Mapping;
use App\Form\MappingType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Entity\Progress;
use App\Service\PartContentParser;

/**
 * Transfer Controller
 * @Route("/api", name="api_")
 */
class TransferController extends AbstractFOSRestController
{
    /**
     * Function prepare Token for given filename and provide it.
     *
     * @param TokenManager $tokenManager manger to handle toklen functions.
     *
     * @return JsonResponse
     *
     * @Route("/transfer/handshake", name="transfer_handshake", methods={"POST"})
     */
    public function handshake(LoggerInterface $logger, TokenManager $tokenManager, Request $request)
    {
        $mappingRequest = $request->get('mapping');
        $entityManager = $this->getDoctrine()->getManager();
        $size = $request->get('file_size');
        $mappingEntity = null;

        if ($mappingRequest['id']) {
            $mappingEntity = $entityManager->getRepository(Mapping::class)
                ->find($mappingRequest['id']);
        }

        if (! $mappingEntity instanceof Mapping) {
            $mappingEntity = new Mapping();
        }

        $form = $this->createForm(MappingType::class, $mappingEntity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            return $this->handleView(
                $this->view(
                    [
                        'token' => $tokenManager->create($mappingEntity, $size)
                    ]
                )
            );
        }

        return $this->handleView(
            $this->view(
                [
                    'code' => 500,
                    'content' => json_encode($form->getErrors())
                ],
                500
            )
        );
    }

    /**
     * Undocumented function
     *
     * @param Request             $request
     *
     * @return void
     *
     * @Route("/transfer/store", name="transfer_store", methods={"POST"})
     *
     */
    public function store(LoggerInterface $logger, TokenManager $tokenManager, PartContentParser $partContentParser, Request $request)
    {
#        session_write_close();
#        session_start();
#        $logger->info(print_r($_SESSION, true));

        /** @var UploadedFile $file */
        $file = $request->files->get('upload');
        $token = $request->get('token');

        $logger->info("Session: ".print_r($request->getSession(), true));
        $logger->info("Upload.progress: ".ini_get('session.upload_progress.enabled').
            "-".ini_get("session.upload_progress.prefix").
            "-".ini_get("session.upload_progress.name"));

#        $logger->info("Content: ".print_r($data, true));


#        $logger->info("CONTENT: ".$request->getContent());
#        $logger->info("Token: ".$token);
#        $logger->info("Request: ".print_r($request->request, true));
#        $logger->info('$_FILE:'.print_r($_FILES, true));
#        $logger->info('$file: '.print_r($file, true));

        try {

            /** Regular file upload found */
            if (!is_null($file)) {
                $tokenManager->validate($token);
                /** @var Progress $progressEntity */
                $progressEntity = $tokenManager->read($token);

                $logger->debug("HABE FILE!");
                $logger->debug(print_r($file, true));

                $progressEntity->setTempPathName($file->getPathname())
                    ->setCurrentSize(0);
                $tokenManager->update($token, $progressEntity);

                $file->move(dirname($progressEntity->getFilePathName()), basename($progressEntity->getFilePathName()));
                
                if (is_readable($progressEntity->getFilePathName())
                    && is_file($progressEntity->getFilePathName())
                ) {
                    $progressEntity->setCurrentSize(filesize($progressEntity->getFilePathName()));
                }
                $tokenManager->update($token, $progressEntity);

                $logger->debug("Token aktualisiert!");
                if (is_readable($progressEntity->getFilePathName())
                    && is_file($progressEntity->getFilePathName())
                ) {
                    $logger->debug("Filesize: ".filesize($progressEntity->getFilePathName()));
                }
            } else {
                if (!empty($request->getContent())) {
                    $data = $partContentParser->parse($request->getContent());
                    $token = $data['token'];
                }
                $tokenManager->validate($token);
                /** @var Progress $progressEntity */
                $progressEntity = $tokenManager->read($token);
                $logger->debug("Habe Stream!");

                $progressEntity->setCurrentSize(0);
                $tokenManager->update($token, $progressEntity);

                $fileHandle = fopen($progressEntity->getFilePathName(), 'w+');

                $logger->info("Schreibe Datei nach : ".$progressEntity->getFilePathName());
                fwrite($fileHandle, $data['upload']);
                fclose($fileHandle);

                if (is_readable($progressEntity->getFilePathName())
                    && is_file($progressEntity->getFilePathName())
                ) {
                    $progressEntity->setCurrentSize(filesize($progressEntity->getFilePathName()));
                }
                $tokenManager->update($token, $progressEntity);
            }
            $tokenManager->delete($token);
            $logger->debug("Durch mit stream schreiben!");
            return $this->handleView($this->view(['code' => 200, 'content' => 'verschieben abgeschlossen']));
        } catch (InvalidTokenException $exception) {
            $logger->error("Transfer Store: ".$exception->getMessage());
            return $this->handleView($this->view(['code' => 500, 'content' => $exception->getMessage()]));
        }
    }

    /**
     * Function prepare Token for given filename and provide it.
     *
     * @Route("/transfer/progress/{token}", name="transfer_progress")
     *
     * @param ProgressManager $progressManager Manager to handle progress of given token.
     * @param string          $token token to identify requested data storage.
     *
     * @return JsonResponse
     */
    public function progressAction(LoggerInterface $logger, Request $request, string $token)
    {
#        session_start();
        
        $logger->info("SESSION VOR CLOSE: ".print_r($request->getSession()->get(ini_get("session.upload_progress.name")), true));
        $logger->info(print_r($_SESSION, true));

        return $this->handleView($this->view(isset($_SESSION['upload_progress_123']) ? $_SESSION['upload_progress_123'] : $_SESSION));

        /*
        return $this->handleView(
            $this->view(
                [
                    'progress' => $progressManager->calculateProgress($token)
                ]
            )
        );
        */
    }

    /**
     * Function prepare Token for given filename and provide it.
     *
     * @Route("/transfer/progress-upload", name="transfer_progress_upload")
     *
     * @param ProgressManager $progressManager Manager to handle progress of given token.
     * @param string          $token token to identify requested data storage.
     *
     * @return JsonResponse
     */
    public function progressUploadAction(LoggerInterface $logger, Request $request)
    {
        $logger->info("SESSION VOR CLOSE: ".print_r($request->getSession()->get(ini_get("session.upload_progress.name")), true));
        $logger->info("SESSION VOR CLOSE: ".print_r($request->getSession()->get(ini_get("session.upload_progress.name"))['content_length'], true));
        $logger->info("SESSION VOR CLOSE: ".print_r($request->getSession()->get(ini_get("session.upload_progress.name"))['bytes_processed'], true));
        return $this->handleView($this->view(['success' => true]));
    }

    /**
     * Recives current progress for given token from given client.
     *
     * @Route("/transfer/progress-send", name="transfer-progress-send")
     *
     */
    public function progressSendAction()
    {
        return $this->render('transfer/progress-send.html.twig');
    }

    /**
     * Undocumented function
     * 
     * @Route("/transfer/phpinfo", name="transfer_phpinfo")
     * 
     * @return void
     */
    public function phpinfoAction()
    {
        $this->render(phpinfo());
    }
}
